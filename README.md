Ansible Basics
=========

Install basics tools/packages for sticluster servers

Requirements
----------

Unattended-upgrades role
git: https://gitlab.epfl.ch/ansible-sti-roles/ansible-unattended-upgrades.git

Should be added to ansible-sti-it requirements.yml, because recursive requirements tree building is not supported by Ansible.